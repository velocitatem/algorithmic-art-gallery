#!/usr/bin/env bash

feh --randomize ~/Documents/Projects/algorithmic-art-gallery/algorithmic-art-gallery/*.jpg --image-bg white  --bg-center
